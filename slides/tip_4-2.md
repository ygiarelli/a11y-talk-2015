### Le mauvais exemple

```html
<a href="…"> » </a>
```

### Mieux

```html
<a href="…">Suivant</a>
```

### Encore mieux

```html
<a href="…" title="Accèder à la page suivante">Suivant</a>
```


Note:

Exemple : pager

2 - Mieux, mais suivant peut être léger dans le contexte
