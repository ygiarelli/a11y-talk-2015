![Chien déguisé en chimiste dans un laboratoire avec l’inscription « I have no idea what i'm doing »](/images/no-idea.jpg)

```html
<img src="/images/no-idea.jpg" 
     alt="Photo d’un chien déguisé en chimiste dans un laboratoire 
          avec l’inscription « I have no idea what i'm doing »
          en haut à droite" />
```

(Valable également pour vidéos / flashs / applets)


Note:

Assez simple, juste chien qui fait le con dans labo, mais déjà de la matière
