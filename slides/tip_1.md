# #1 

### Tous les contenus non-textuels doivent avoir une alternative textuelle (cohérente)  
<br />

 * Images
 * Vidéos
 * Sons
 * Flash et… Aheum… Java


Note:

Mais aussi globalement tout ce qui est canvas, object, embed, svg, etc…

Le contenu textuel doit être long si besoin !

Pour du contenu relatif au temps (audio, vidéo), il faut forcément mettre le paquet ! 
