# #2 

### Les textes doivent être (vraiment) lisibles  
<br />

 * Contraste suffisant
 * Attention aux couleurs (daltonisme)
 * Le site ne casse pas à 200% de zoom
 * Pas de textes dans des images 
 * Limiter la longueur des lignes

Note:
Contraste 7:1, la couleur ne suffit pas à contraster !
Texte dans image possible mais déco.
Ligne 80 symboles.
