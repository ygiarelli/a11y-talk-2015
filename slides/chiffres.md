# Quelques chiffres :

En france, on recense près de **12 millions de personnes en situation de handicap** dont :

 * 1,7 million de malvoyants
 * 5,2 millions de malentendants
 * 2,3 millions de handicapés moteur
 * 2% d'utilisateurs d'IE8

Note:

Désolé, pas de chiffre absolu pour IE
