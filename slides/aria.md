## Les références à connaître
<br />

### WCAG (Web Content Accessibility Guidelines)  
Recommandations du W3C à tous les niveaux pour l’accessibilité. Complet et… accessible.  
<br />

### RGAA (Référentiel Général d’Accessibilité pour les Administration)  
Text Français d'application du WCAG  
<br />

### ARIA (Accessible Rich Internet Applications)  
Technologie de transition destinée à rendre accessible les application web dites «&nbsp;Riches&nbsp;»


